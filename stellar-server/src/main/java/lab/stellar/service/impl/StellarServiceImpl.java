package lab.stellar.service.impl;

import lab.stellar.dao.PlanetDAO;
import lab.stellar.dao.SystemDAO;
import lab.stellar.entities.Planet;
import lab.stellar.entities.PlanetarySystem;
import lab.stellar.service.StellarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.logging.Logger;

@Service
public class StellarServiceImpl implements StellarService {


    Logger logger = Logger.getLogger(StellarServiceImpl.class.getName());

    @Autowired
    private SystemDAO systemDAO;

    @Autowired
    private PlanetDAO planetDAO;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Override
    public List<PlanetarySystem> getSystems() {
        logger.info("fetching all planetary systems");
        List<PlanetarySystem> systems = systemDAO.getAllPlanetarySystems();
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public List<PlanetarySystem> getSystemsByName(String like) {
        logger.info("fetching planetary systems like " + like);
        List<PlanetarySystem> systems =  systemDAO.getPlanetarySystemsByName(like);
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public PlanetarySystem getSystemById(int id) {
        logger.info("fetching planetary system by id " + id);

        PlanetarySystem system = systemDAO.getPlanetarySystem(id);
        logger.info("found: " + system);
        return  system;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem s) {
        logger.info("fetching  planets by system " + s);

        List<Planet> planets = planetDAO.getPlanetsBySystem(s);
        logger.info("found:" + planets.size());
        return planets;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem system, String like) {
        return planetDAO.getPlanetsBySystemAndName(system,like);
    }


    @Override
    public Planet getPlanetById(int id) {
        return planetDAO.getPlanetById(id);
    }

    @Override
    public Planet addPlanet(Planet p, PlanetarySystem s) {
        p.setSystem(s);
        return planetDAO.addPlanet(p);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    @PreAuthorize("hasRole('ADMIN')")
    public PlanetarySystem addPlanetarySystem(PlanetarySystem s) {

       /* TransactionTemplate tt = new TransactionTemplate(transactionManager);
        return tt.execute(transactionStatus -> {*/
            boolean failure = false;
            PlanetarySystem system = systemDAO.addPlanetarySystem(s);

            if(failure) {
               throw new IllegalStateException("fake exception");
            }
            return system;
        //});

       /* TransactionStatus ts = transactionManager.getTransaction(new DefaultTransactionDefinition());
        boolean failure = false;
        try{
            PlanetarySystem system = systemDAO.addPlanetarySystem(s);

            if(failure) {
                throw new IllegalStateException("fake exception");
            }

            transactionManager.commit(ts);
            return system;
        }catch (Exception e){
            transactionManager.rollback(ts);
            throw e;
        }*/
    }


    public void setPlanetDAO(PlanetDAO planetDAO) {
        this.planetDAO = planetDAO;
    }

    public void setSystemDAO(SystemDAO systemDAO) {
        this.systemDAO = systemDAO;
    }
}
