package lab.stellar.service;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class SloganService {

    @Resource
    private List<String> slogans;

    public String getSlogan(){
        return slogans.get(new Random(new Date().getTime()).nextInt(slogans.size()));
    }

}
