package lab.stellar.web;

import lab.stellar.entities.Planet;
import lab.stellar.entities.PlanetarySystem;
import lab.stellar.service.StellarService;
import lab.stellar.validation.SystemValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

@RestController
@RequestMapping("/webapi")
public class StellarREST {

    Logger logger = Logger.getLogger(StellarREST.class.getName());

    @Autowired
    StellarService stellarService;

    @Autowired
    SystemValidator systemValidator;

    @Autowired
    MessageSource messageSource;

  /*  @InitBinder
    void initBinder(WebDataBinder binder){
        binder.setValidator(systemValidator);
    }
*/
    @GetMapping(value = "/systems", produces = "application/*")
    public List<PlanetarySystem> getSystems(){
        logger.info("fetching all systems");
        return stellarService.getSystems();
    }

    @GetMapping("/systems/{id}")
    public ResponseEntity<PlanetarySystem> getSystem(@PathVariable int id){
        PlanetarySystem system = stellarService.getSystemById(id);

        return system!=null ?
              ResponseEntity.status(HttpStatus.OK).body(system) :
              ResponseEntity.notFound().build();
    }

    // /systems/{id}/planets

    @GetMapping("/systems/{id}/planets")
    ResponseEntity<List<Planet>> getPlanets(@PathVariable("id") int systemId){
        PlanetarySystem system = stellarService.getSystemById(systemId);
        if(system==null){
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(stellarService.getPlanets(system), HttpStatus.OK);
    }


    @PostMapping("/systems")
    ResponseEntity addSystem(@Validated @RequestBody PlanetarySystem system, BindingResult br){

        if(br.hasErrors()){
            StringBuilder sb = new StringBuilder();
            for(FieldError fe : br.getFieldErrors()){
                sb.append(messageSource.getMessage(fe.getCode(), new Object[]{}, Locale.getDefault()));
            }
            return ResponseEntity.badRequest().body(sb.toString());
        }
        return new ResponseEntity<>(stellarService.addPlanetarySystem(system), HttpStatus.CREATED);
    }

}
