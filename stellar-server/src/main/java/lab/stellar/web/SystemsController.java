package lab.stellar.web;

import lab.stellar.entities.PlanetarySystem;
import lab.stellar.service.SloganService;
import lab.stellar.service.StellarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.logging.Logger;

@Controller
public class SystemsController {

    private Logger logger = Logger.getLogger(SystemsController.class.getName());

    @Autowired
    private StellarService stellarService;


    @GetMapping(path="/systems")
    public String getSystems(Model model,
                             @RequestParam(value = "phrase", required = false) String phrase,
                             @RequestHeader(value = "User-Agent", required = false) String userAgent,
                             @CookieValue(value = "JSESSIONID", required = false) String sessionId,
                             HttpServletRequest request,
                             @RequestHeader HttpHeaders headers){
        logger.info("about to fetch planetary systems");
        logger.info("user-agent: " + userAgent);
        logger.info("session id:" + sessionId);
        logger.info("context path: " + request.getContextPath());

        List<PlanetarySystem> systems = phrase==null ?
                stellarService.getSystems() : stellarService.getSystemsByName(phrase);
        model.addAttribute("systems", systems);

        return "systems";
    }

    @GetMapping(path="/addSystem")
    String addSystemPrepare(Model model){
        logger.info("about to prepare adding system form");
        model.addAttribute("systemForm", new PlanetarySystem());
        return "addSystem";
    }

    @PostMapping(path="/addSystem")
    String addSystem(@ModelAttribute("systemForm") PlanetarySystem system, BindingResult br){

        if(system.getName()==null || system.getName().trim().isEmpty()){
            br.rejectValue("name", "error.name.empty");
        }
        if(system.getDistance()<=0){
            br.rejectValue("distance", "error.distance.negative");
        }
        if(br.hasErrors()){
            return "addSystem";
        }
        stellarService.addPlanetarySystem(system);
        return "redirect:/systems";
    }

}
