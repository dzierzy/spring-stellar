package lab.stellar.web;

import lab.stellar.entities.Planet;
import lab.stellar.service.StellarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.logging.Logger;

@Controller
public class PlanetsController {

    Logger logger = Logger.getLogger(PlanetsController.class.getName());

    @Autowired
    private StellarService stellarService;

    // /stellar/planets?systemId=1
    @GetMapping(path = "/planets")
    public String getPlanetsBySystem(Model model, @RequestParam("systemId") int systemId){
        logger.info("about to fetch planets of planetary system " + systemId);
        List<Planet> planets = stellarService.getPlanets(stellarService.getSystemById(systemId));
        model.addAttribute("planets", planets);
        return "planets";
    }
}
