package lab.stellar.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.logging.Logger;

@Controller
public class HelloController {


    private Logger logger = Logger.getLogger(HelloController.class.getName());

    //@RequestMapping(path = "/hello", method = RequestMethod.GET)
    @GetMapping(path = "/hello")
    public String sayHello(Model model){
        logger.info("hello request received.");
        model.addAttribute("hello_text","Hey You!");
        return "hello";
    }
}
