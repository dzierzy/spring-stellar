package lab.stellar.validation;

import lab.stellar.entities.PlanetarySystem;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class SystemValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(PlanetarySystem.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PlanetarySystem system = (PlanetarySystem) o;
        /*if(system.getName()==null || system.getName().trim().isEmpty()){
            errors.rejectValue("name", "error.name.empty");
        }*/
        if(system.getDistance()<=0){
            errors.rejectValue("distance", "error.distance.negative");
        }
    }
}
