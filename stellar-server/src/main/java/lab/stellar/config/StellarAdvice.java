package lab.stellar.config;

import lab.stellar.service.SloganService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.logging.Logger;

@ControllerAdvice
public class StellarAdvice {

    Logger logger = Logger.getLogger(StellarAdvice.class.getName());

    @Autowired
    SloganService sloganService;

    @ModelAttribute
    public void addSlogan(Model model){
        logger.info("adding slogan");
        model.addAttribute("slogan", sloganService.getSlogan());
    }

    @ExceptionHandler(Exception.class)
    public String handleException(Exception e, Model model){
        model.addAttribute("error_message", e.getMessage());
        e.printStackTrace();
        return "error";
    }
}
