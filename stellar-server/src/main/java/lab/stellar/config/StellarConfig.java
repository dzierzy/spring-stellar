package lab.stellar.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class StellarConfig {

    @Bean("slogans")
    List<String> getSlogans(){
        return Arrays.asList(
                "Sky is the limit",
                "Per aspera ad astra",
                "itp itd"
        );
    }
}
