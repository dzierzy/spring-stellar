package lab.stellar.start;

import lab.stellar.dao.SystemDAO;
import lab.stellar.entities.PlanetarySystem;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ComponentScan("lab.stellar")
public class StellarDataStart {

    public static void main(String[] args) {

        System.out.println("StellarDataStart.main");

        ApplicationContext context =
                new AnnotationConfigApplicationContext( StellarDataStart.class );

        SystemDAO systemDAO = context.getBean(SystemDAO.class);
        List<PlanetarySystem> systems = systemDAO.getAllPlanetarySystems();

        systems.forEach( s -> System.out.println(s) );

    }
}
