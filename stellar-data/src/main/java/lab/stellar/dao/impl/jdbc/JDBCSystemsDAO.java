package lab.stellar.dao.impl.jdbc;

import lab.stellar.dao.SystemDAO;
import lab.stellar.entities.PlanetarySystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class JDBCSystemsDAO implements SystemDAO {

    public static final Logger logger = Logger.getLogger(JDBCSystemsDAO.class.getName());


    public static final String SELECT_ALL_SYSTEMS = "select ps.id as system_id, ps.details as system_details, " +
            "ps.name as system_name, ps.distance as system_distance, ps.discovery as system_discovery from planetarysystem ps";

    public static final String SELECT_SYSTEMS_BY_NAME = "select ps.id as system_id, ps.details as system_details, "+
                "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance from planetarysystem ps where name like ?";

    public static final String SELECT_SYSTEM_BY_ID = "select ps.id as system_id, ps.details as system_details, "+
            "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance from planetarysystem ps where id=?";



    @Autowired
    DataSource dataSource;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        List<PlanetarySystem> systems =
            jdbcTemplate.query(SELECT_ALL_SYSTEMS, new SystemMapper());
        /*new ArrayList<>();
        try(Connection con = this.dataSource.getConnection();
            Statement statement = con.createStatement();) {
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_SYSTEMS);
            while (resultSet.next()) {
                systems.add(mapSystem(resultSet));
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }*/
        return systems;
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        List<PlanetarySystem> systems =
                jdbcTemplate.query(SELECT_SYSTEMS_BY_NAME, new Object[]{"%" + like + "%"}, new SystemMapper());

                /*new ArrayList<>();
        try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(SELECT_SYSTEMS_BY_NAME)) {
            prpstm.setString(1, "%" + like + "%");
            ResultSet rs = prpstm.executeQuery();
            while (rs.next()) {
                systems.add(mapSystem(rs));
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }*/
        return systems;
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        PlanetarySystem ps =
                jdbcTemplate.queryForObject(SELECT_SYSTEM_BY_ID, new Object[]{id}, new SystemMapper());
       /* try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(SELECT_SYSTEM_BY_ID)) {
            prpstm.setInt(1, id);
            ResultSet rs = prpstm.executeQuery();
            if(rs.next()) {
                ps = mapSystem(rs);
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }*/
        return ps;
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {

        jdbcTemplate.update(
                "INSERT INTO PLANETARYSYSTEM(NAME, STAR, DISTANCE, DISCOVERY) VALUES (?,?,?,?)",
                system.getName(), system.getStar(), system.getDistance(), system.getDiscovery());

        return system;
    }

}
